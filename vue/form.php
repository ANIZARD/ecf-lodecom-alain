<!doctype html>
<html lang="fr">
<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  
  <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>

  <link rel="stylesheet" href="style.css">

  <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
  <title>Création de structure</title>
</head>

<body>

  <!--Menu Gauche-->
  <div id="app" class="container col col-sm-6">
    <form method="post" :action="'switch.php?form=set&client_id='+idPartenaire">

      <div class="row">
        <div class="col-sm-6">

          <div class="form-group row d-flex">
            <label class="form-label">Install_id</label>
            <input class="form-control " name="installId" type="text">
          </div>

          <div class="form-group row">
            <label class="form-label">Branch_Id</label>
            <input class="form-control" name="branchid" type="text">
          </div>


        </div> 
      <!--Colonne row fermeture-->
       </div>

    <button type="submit" class="btn btn-success text-center">Valider</button>
  </form>
</div> 



<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>

<script>

  var idPartenaire = "<?php echo $clientId; ?>";

</script>           
<script>

  var app = new Vue(
    {
      el: "#app",
      data: function(){
        return {idPartenaire};

      }
    }
  )
</script>
</body>
</html>
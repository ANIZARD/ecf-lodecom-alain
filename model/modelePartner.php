<?php
// Author : vincent vorillion

 // reqSQL selectionne tous les partenaires.
	function MODELE_getPartners() {
        global $bdd;
        $reqSQL='SELECT * FROM api_client';
        $requete=$bdd->query($reqSQL);
        $lesTuples = $requete->fetchAll();
        $requete->closeCursor();
    
        return $lesTuples;
    }

 // reqSQL selectionne un partenaire par son client_id   

 function MODELE_getPartnerclientid($clientid) {
    global $bdd;
    $reqSQL='SELECT * FROM api_client
    WHERE client_id = :id';

    $requete = $bdd->prepare($reqSQL);         
    $requete->bindValue(':id', $clientid, PDO::PARAM_STR);
    $requete->execute();

    $leTuple = $requete->fetch();
    $requete->closeCursor();

    return $leTuple;
}
 // reqSQL selectionne un partenaire par son client_name
 
 function MODELE_getPartnerclientname($clientname) {
    global $bdd;
    $reqSQL='SELECT * FROM api_client
    WHERE client_name = :id';

    $requete = $bdd->prepare($reqSQL);         
    $requete->bindValue(':id', $clientname);
    $requete->execute();

    $leTuple = $requete->fetch();
    $requete->closeCursor();

    return $leTuple;
 }

// reqSQL selectionne tous les partenaires actifs

function MODELE_getPartnersclientactif($activey) {
    global $bdd;
    $reqSQL='SELECT * FROM api_client
    WHERE active = :Y';

    $requete = $bdd->prepare($reqSQL);         
    $requete->bindValue(':Y', $activey);
    $requete->execute();

    $lesTuples = $requete->fetchAll();
    $requete->closeCursor();

    return $lesTuples;
 }

// reqSQL selectionne tous les partenaires inactifs

function MODELE_getPartnersclientinactif($activen) {
    global $bdd;
    $reqSQL='SELECT * FROM api_client
    WHERE active = :N';

    $requete = $bdd->prepare($reqSQL);         
    $requete->bindValue(':N', $activen);
    $requete->execute();

    $lesTuples = $requete->fetchAll();
    $requete->closeCursor();

    return $lesTuples;
 }

// reqSQL switch partenaire en actif/inactif

function MODELE_getPartnerclientswitch($activate,$id) {
    global $bdd;
    $reqSQL='UPDATE api_client SET active=:active WHERE client_id = :clientid';
    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':active', $activate);
    $requete->bindValue(':clientid', $id);
    $requete->execute();
	$requete->closeCursor();
}
function MODELE_getSearch($clientid) {
    global $bdd;
    $reqSQL="SELECT * FROM api_client
    WHERE client_id like concat('%',:clientid,'%') OR client_name like concat('%',:clientname,'%')";

    $requete = $bdd->prepare($reqSQL);         
    $requete->bindValue(':clientid', $clientid, PDO::PARAM_STR);
    $requete->bindValue(':clientname', $clientid, PDO::PARAM_STR);
    $requete->execute();

    $leTuple = $requete->fetchAll();
    $requete->closeCursor();

    return $leTuple;
}

?>
